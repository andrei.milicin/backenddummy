package net.toomuchram.backendDummy

import net.toomuchram.backendDummy.managers.*
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import java.sql.SQLException
import java.util.*

class TUI {
    fun createTables() {
        val tables: List<Table> = listOf(
            Tables.Excursions,
            Tables.Announcements,
            Tables.DeviceTokens,
            Tables.Users,
            Tables.Sessions,
            Tables.Msg,
            Tables.HtmlContent,
            Tables.TeamCategories,
            Tables.TeamMembers,
            Tables.Teams,
            Tables.CourseContent,
            Tables.Images
        )
        println("Creating tables...")
        try {
            transaction {
                for (table in tables) {
                    SchemaUtils.createMissingTablesAndColumns(table)
                }
            }
        } catch (e: SQLException) {
            println("Something related to MySQL/MariaDB has gone wrong. Please check if the database credentials listed in applicationConfig.properties are correct.")
        } catch (e: Exception) {
            println("Something has gone terribly wrong")
            e.printStackTrace()
            return
        }

        println("Creating the default team category...")
        val userManager = UserManager()
        val teamsManager = TeamsManager(userManager)
        transaction {
            Tables.Excursions.slice(Tables.Excursions.id).selectAll().forEach { row ->
                try {
                    teamsManager.createTeamCategory("default", row[Tables.Excursions.id].value)
                } catch (e: Exceptions.AlreadyExistsException) {
                    println("Category already exists.")
                } catch (e: Exception) {
                    println("Something went wrong while trying to create the default team category.")
                }
            }
        }
    }

    fun createExcursion(args: Array<String>) {
        var argsIndex: Int? = null
        args.forEachIndexed { index, s ->
            if (s == "--create-excursion") argsIndex = index
        }
        val excursionName = args[argsIndex!!+1]

        val userManager = UserManager()
        val excursionManager = ExcursionManager()

        val id = excursionManager.addExcursion(excursionName)


        val admin = Models.User(
            "admin",
            UUID.randomUUID().toString(),
            id
        )

        userManager.register(admin, true)

        println("Password: " + admin.password)
    }
}