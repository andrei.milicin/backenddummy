package net.toomuchram.backendDummy.responders

import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import net.toomuchram.backendDummy.managers.Exceptions
import net.toomuchram.backendDummy.responders.Helpers
import net.toomuchram.backendDummy.responders.Managers

fun Routing.teams(managers: Managers, helpers: Helpers) {
    get("/getTeams") {
        val queryParameters = call.request.queryParameters
        val excursionId = helpers.excursionIdFromSession(queryParameters["sessionId"]!!)
        val teams = if (queryParameters.contains("categoryId")) {
            val categoryId = queryParameters["categoryId"]!!.toInt()
            managers.teamsManager.listTeams(excursionId, categoryId)
        } else {
            managers.teamsManager.listTeams(excursionId)
        }

        val response = mutableListOf<TeamResponse>()
        teams.map { team ->
            response.add(
                TeamResponse(
                    team.id, team.name, team.category
                )
            )
        }
        call.respond(response)

    }

    get("/getTeamCategories") {
        val categories = mutableListOf<TeamCategoryResponse>()
        val excursionId = helpers.excursionIdFromSession(call.request.queryParameters["sessionId"]!!)
        managers.teamsManager.listTeamCategories(excursionId).map { category ->
            val (categoryid, categoryName) = category
            categories.add(
                TeamCategoryResponse(
                    categoryid, categoryName
                )
            )
        }
        call.respond(categories)
    }

    get("/getTeamMembers") {
        val queryParameters = call.request.queryParameters
        if (!queryParameters.contains("teamId")) {
            call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_VARIABLES")
            return@get finish()
        }
        try {
            val teamId = queryParameters["teamId"]!!.toInt()
            val teamMembersByUserID = managers.teamsManager.getTeamMembers(teamId)
            val teamMembers = mutableListOf<TeamMemberResponse>()
            teamMembersByUserID.map { userId ->
                teamMembers.add(
                    TeamMemberResponse(
                        id = userId,
                        name = managers.userManager.getUsername(userId)
                    )
                )
            }
            call.respond(teamMembers)

        } catch (e: Exceptions.NoSuchTeamException) {
            call.respond(HttpStatusCode.BadRequest, "ERR_NO_SUCH_TEAM")
        } catch (e: Exception) {
            call.respond(HttpStatusCode.InternalServerError, "ERR_INTERNAL")
            e.printStackTrace()
        }

    }
}