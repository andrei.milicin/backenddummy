package net.toomuchram.backendDummy.responders

import net.toomuchram.backendDummy.managers.*
import net.toomuchram.backendDummy.websocket.RomereisWebsocket

class Managers(
    val adminTasks: AdminTasks,
    val announcementManager: AnnouncementManager,
    val chatManager: ChatManager,
    val configManager: ConfigManager,
    val excursionManager: ExcursionManager,
    val htmlContentManager: HtmlContentManager,
    val sessionManager: SessionManager,
    val userManager: UserManager,
    val teamsManager: TeamsManager,
    val websocket: RomereisWebsocket
) {

}