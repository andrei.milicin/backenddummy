package net.toomuchram.backendDummy.managers

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.Table
import org.joda.time.DateTime
import java.sql.Blob

class Tables {
    object Announcements : Table() {
        val time: Column<DateTime> = datetime("time")
        val message: Column<String> = varchar("message", 255)
        val username: Column<String> = varchar("username", 255)
        val excursion: Column<EntityID<Int>> = reference("excursionId", Excursions, ReferenceOption.CASCADE, ReferenceOption.CASCADE)
    }

    object DeviceTokens : Table() {
        val userId: Column<EntityID<Int>> = reference("userId", Users, ReferenceOption.CASCADE, ReferenceOption.CASCADE)
        val token: Column<String> = varchar("token", 255)
        val sessionId: Column<String> = reference("sessionId", Sessions.sessionId)
        val deviceType: Column<String> = varchar("deviceType", 255)
    }

    object Users : IntIdTable() {
        val username: Column<String> = varchar("username", 255)
        val password: Column<String> = varchar("password", 255)
        val admin: Column<Boolean> = bool("admin")
        val excursion: Column<EntityID<Int>> = reference("excursionId", Excursions, ReferenceOption.CASCADE, ReferenceOption.CASCADE)
        init {
            index(true, username, excursion) // case 1 - Unique index
        }
    }

    object Msg : Table() {
        val messageId: Column<Int> = integer("messageId")
        val teamId: Column<EntityID<Int>?> = reference("teamId", Teams, ReferenceOption.CASCADE, ReferenceOption.CASCADE).nullable()
        val timestamp: Column<DateTime> = datetime("timestamp")
        val img: Column<Blob> = blob("img")
        val contentType: Column<String> = varchar("content-type", 100)
        val user: Column<EntityID<Int>?> = reference("userId", Users, ReferenceOption.SET_NULL, ReferenceOption.CASCADE).nullable()
        val message: Column<String> = varchar("message", 255)
        val excursion: Column<EntityID<Int>> = reference("excursionId", Excursions, ReferenceOption.CASCADE, ReferenceOption.CASCADE)
    }

    object HtmlContent: IntIdTable() {
        val name: Column<String> = varchar("name", 50)
        val content: Column<String> = text("content")
        val excursionId: Column<EntityID<Int>> = reference("excursionId", Excursions, ReferenceOption.CASCADE, ReferenceOption.CASCADE)

        init {
            index(true, name, excursionId)
        }
    }

    object Sessions: Table() {
        val sessionId: Column<String> = varchar("sessionId", 255).uniqueIndex()
        val userId: Column<EntityID<Int>> = reference("userId", Users, ReferenceOption.CASCADE, ReferenceOption.CASCADE)
    }

    object TeamCategories : IntIdTable() {
        val name: Column<String> = varchar("name", 255)
        val excursionId: Column<EntityID<Int>> = reference("excursionId", Excursions, ReferenceOption.CASCADE, ReferenceOption.CASCADE)
    }

    object Teams : IntIdTable() {
        val name: Column<String> = varchar("name", 255)
        val category: Column<EntityID<Int>> = reference("category", TeamCategories, ReferenceOption.CASCADE, ReferenceOption.CASCADE)
    }

    object TeamMembers: Table() {
        val userId: Column<EntityID<Int>> = reference("userId", Users, ReferenceOption.CASCADE, ReferenceOption.CASCADE)
        val teamId: Column<EntityID<Int>> = reference("teamId", Teams, ReferenceOption.CASCADE, ReferenceOption.CASCADE)

        init {
            index(true, userId, teamId)
        }
    }

    object Excursions: IntIdTable() {
        val name: Column<String> = varchar("name", 255)
    }

    object CourseContent: IntIdTable() {
        val name: Column<String> = varchar("name", 255)
        val category: Column<String> = varchar("category", 100) // Aantekeningen, Huiswerk, etc
        val section: Column<String> = varchar("section", 255) // "Les 1", "Les 2", etc. "Geotracker" for Geotracker content
        val contentId: Column<EntityID<Int>> = reference("contentId", HtmlContent)
        val latitude: Column<Double?> = double("lat").nullable()
        val longitude: Column<Double?> = double("lon").nullable()
        val excursionId: Column<EntityID<Int>> = reference("excursionId", Excursions)

        init {
            index(true, name, category, section)
        }
    }

    object Images: IntIdTable() {
        val contentId: Column<EntityID<Int>> = reference("contentId", HtmlContent)
        val image: Column<Blob> = blob("image")
        val contentType: Column<String> = varchar("contentType", 100)
    }

}