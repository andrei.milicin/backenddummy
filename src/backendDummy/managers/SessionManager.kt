package net.toomuchram.backendDummy.managers


import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import kotlin.random.Random

// This class manages the table `Sessions`
class SessionManager {

    private val entityIDConverter = EntityIDConverter()


    // Based on https://www.baeldung.com/kotlin-random-alphanumeric-string
    /**
     * Function to generate a session ID
     * @param length The length of the session ID to be generated
     * @return A session ID
     */
    private fun generateSessionId(length: Int = 60): String {
        val charPool : List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

        return (1..length)
            .map { Random.nextInt(0, charPool.size) }
            .map(charPool::get)
            .joinToString("")
    }

    /**
     * Function to create a new session
     * @param userId The user ID, identifying the user to which the session belongs
     * @return The session ID of the new session
     * I can't believe it's this simple.
     */
    fun createSession(userId: Int): String {
        val userEntityId = entityIDConverter.getUserEntityID(userId)

        val sessionId = generateSessionId()
        transaction {
            Tables.Sessions.insert {
                it[Tables.Sessions.sessionId] = sessionId
                it[Tables.Sessions.userId] = userEntityId
            }
        }
        return sessionId
    }

    /**
     * Function to end a session
     * @param sessionId The ID of the session to be ended
     */
    fun endSession(sessionId: String) {
        transaction {
            Tables.Sessions.deleteWhere { Tables.Sessions.sessionId eq sessionId }
        }
    }

    /**
     * Function to end all sessions tied to a userId
     * @param userId The ID of the user
     */
    fun endAllSessions(userId: Int) {
        transaction {
            Tables.Sessions.deleteWhere { Tables.Sessions.userId eq userId }
        }
    }

    /**
     * Function to verify if a session exists
     * @param sessionId The session ID of the session to be verified
     * @return A boolean representing the session validity
     */
    fun verifySession(sessionId: String): Boolean {
        return transaction {
            val sessions = Tables.Sessions.select { Tables.Sessions.sessionId eq sessionId}
            return@transaction sessions.count() > 0
        }
    }

    /**
     * Function to retrieve the user ID belonging to a session ID
     * @param sessionId The sessionID of the session
     * @return The user ID
     */
    fun getUserId(sessionId: String): Int {
        // TODO: Fix new table layout
        return transaction {
            val sessions = Tables.Sessions.slice(Tables.Sessions.userId).select {
                Tables.Sessions.sessionId eq sessionId
            }

            if(sessions.count() <= 0) {
                throw Exceptions.NoSuchSessionException("No such session found")
            } else {
                return@transaction sessions.elementAt(0)[Tables.Sessions.userId].value
            }

        }
    }
}
