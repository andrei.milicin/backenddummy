package net.toomuchram.backendDummy.managers

import com.google.gson.JsonObject
import com.turo.pushy.apns.ApnsClient
import com.turo.pushy.apns.PushNotificationResponse
import com.turo.pushy.apns.util.ApnsPayloadBuilder
import com.turo.pushy.apns.util.SimpleApnsPushNotification
import com.turo.pushy.apns.util.concurrent.PushNotificationFuture
import com.turo.pushy.apns.util.concurrent.PushNotificationResponseListener
import io.ktor.http.cio.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.pixsee.fcm.Message
import org.pixsee.fcm.Notification
import org.pixsee.fcm.Sender
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


// This class manages two tables:
// Announcements and DeviceTypes
class AnnouncementManager(private val configManager: ConfigManager, val apnsClient: ApnsClient?) {

    private val entityIDConverter = EntityIDConverter()

    fun registerDeviceToken(userId: Int, token: String, sessionId: String, devicetype: String) {

        // Check if the token is already present in the database
        val alreadyRegistered = transaction {
            return@transaction Tables.DeviceTokens.select {
                (Tables.DeviceTokens.token eq token) and (Tables.DeviceTokens.deviceType eq devicetype)
            }.count() > 0
        }
        if (alreadyRegistered) {
            return
        }

        val userEntityId = entityIDConverter.getUserEntityID(userId)

        // TODO: Fix new table layout
        transaction {
            Tables.DeviceTokens.insert {
                it[Tables.DeviceTokens.userId] = userEntityId
                it[Tables.DeviceTokens.token] = token
                it[Tables.DeviceTokens.sessionId] = sessionId
                it[deviceType] = devicetype
            }
        }
    }

    fun deleteAllDeviceTokens(userId: Int) {
        transaction {
            Tables.DeviceTokens.deleteWhere {
                Tables.DeviceTokens.userId eq userId
            }
        }
    }

    /**
     * Function to get the device token and device type for a particular user
     * @param users A list of users for which to get the device tokens
     * @return a list or pairs of device tokens and device types
     *//*
    fun getDeviceTokensForUsers(users: Iterable<Int>): List<Pair<String, String>> {
        return transaction {
            val tokens = mutableListOf<Pair<String, String>>()

            Tables.DeviceTokens.slice(Tables.DeviceTokens.token, Tables.DeviceTokens.deviceType).select {
                    Tables.DeviceTokens.userId inList users
                }.forEach {
                    tokens.add(
                        Pair(
                            it[Tables.DeviceTokens.token], it[Tables.DeviceTokens.deviceType]
                        )
                    )
                }
            return@transaction tokens
        }
    }*/

    /**
     * Function to get a list of all announcements
     * @return The list of announcements
     */
    fun getAllAnnouncements(excursionId: Int): List<Models.Announcement> {
        return transaction {
            val announcements = mutableListOf<Models.Announcement>()
            Tables.Announcements.select { Tables.Announcements.excursion eq excursionId }.forEach {
                announcements.add(
                    Models.Announcement(
                        notiftime = it[Tables.Announcements.time].toLocalDateTime().toString("dd-MM-yyyy hh:mm:ss"),
                        message = it[Tables.Announcements.message],
                        username = it[Tables.Announcements.username]
                    )
                )
            }
            return@transaction announcements
        }
    }

    /**
     * Function to send an announcement and store it in the database
     * @param message The message to be send to all users of the app
     */
    fun sendNotification(message: String, username: String, excursionId: Int) {

        // Since storing the announcement does not require any more functionality,
        // it's done in a coroutine so the main thread can continue on with actually sending the message
        CoroutineScope(Dispatchers.Default).launch {
            transaction {
                Tables.Announcements.insert {
                    it[time] = DateTime.now(
                        DateTimeZone.forTimeZone(
                            TimeZone.getTimeZone("Europe/Amsterdam")
                        )
                    )
                    it[Tables.Announcements.message] = message
                    it[Tables.Announcements.username] = username
                    it[excursion] = entityIDConverter.getExcursionEntityID(excursionId)
                }
            }
        }

        val fcmTokens = transaction {
            val tokens: MutableList<String> = mutableListOf()
            (Tables.DeviceTokens innerJoin Tables.Users).slice(Tables.DeviceTokens.token).select {
                    (Tables.DeviceTokens.deviceType eq "Android") and
                            (Tables.Users.excursion eq excursionId) and
                            (Tables.Users.id eq Tables.DeviceTokens.userId)
                }.forEach {
                    tokens.add(it[Tables.DeviceTokens.token])
                }

            return@transaction tokens
        }
        CoroutineScope(Dispatchers.IO).launch {
            sendAndroidMessage(message, fcmTokens)
        }

        val apnsTokens = transaction {
            val tokens: MutableList<String> = mutableListOf()
            Tables.DeviceTokens.slice(Tables.DeviceTokens.token).select {
                    Tables.DeviceTokens.deviceType eq "iOS"
                }.forEach {
                    tokens.add(it[Tables.DeviceTokens.token])
                }

            return@transaction tokens
        }
        CoroutineScope(Dispatchers.IO).launch {
            sendiOSMessage(message, apnsTokens)
        }

    }

    /**
     * ====================
     * | FCM RELATED CODE |
     * ====================
     */

    /**
     * Function to send a notification to devices running Android through FCM
     * @param message The message to be sent
     * @param tokens The device tokens to which to send the message
     */
    private suspend fun sendAndroidMessage(
        message: String, tokens: List<String>
    ) {
        withContext(Dispatchers.IO) {
            val config = configManager.readFcmConfig()
            val apikey = config.apikey
            val msgSound = config.announcementSound
            val packageName = config.packageName
            val title = config.announcementTitle

            val sender = Sender(apikey)

            for (token in tokens) {
                val notification = Notification(
                    title, message
                )
                notification.sound = msgSound

                val fcmMessage =
                    Message.MessageBuilder().toToken(token).notification(notification).priority(Message.Priority.HIGH)
                        .restrictedPackageName(packageName).build()

                sender.send(fcmMessage, FCMCallback(token))
            }
        }
    }

    class FCMCallback(private val token: String) : Callback<JsonObject> {
        override fun onFailure(call: Call<JsonObject>?, t: Throwable?) {
            t?.printStackTrace()
        }

        override fun onResponse(call: Call<JsonObject>?, response: Response<JsonObject>?) {
            val success: Int = response?.body()?.get("success").toString().toInt()
            if (success == 0) {
                val error = response?.body()?.getAsJsonArray("results")?.get(0)?.asJsonObject?.get("error").toString()
                println(response?.body())

                if (error == "\"NotRegistered\"" || error == "\"InvalidRegistration\"") {
                    transaction {
                        Tables.DeviceTokens.deleteWhere {
                            Tables.DeviceTokens.token eq token
                        }
                    }
                }
            }
        }
    }

    /**
     * =====================
     * | APNS RELATED CODE |
     * =====================
     */

    /**
     * Function to send announcements to iOS devices through Apple Push Notification Service
     * @param message Them message to be send
     * @param tokens A list of device tokens to which to send the notification
     */

    private suspend fun sendiOSMessage(
        message: String, tokens: List<String>
    ) = CoroutineScope(Dispatchers.IO).launch {
        if (apnsClient == null) {
            return@launch
        }
        withContext(Dispatchers.IO) {
            val config = configManager.readAPNsConfig()
            val title = config.announcementTitle


            val payloadBuilder = ApnsPayloadBuilder()
            payloadBuilder.setAlertTitle(title)
            payloadBuilder.setSound(config.announcementSound)
            payloadBuilder.setBadgeNumber(1)
            payloadBuilder.setAlertBody(message)
            val payload = payloadBuilder.buildWithDefaultMaximumLength()

            for (token in tokens) {
                val pushNotification = SimpleApnsPushNotification(token, config.packageName, payload)
                val sendNotificationFuture = apnsClient.sendNotification(pushNotification)
                sendNotificationFuture.addListener(APNsCallback())
            }
        }
    }

    class APNsCallback : PushNotificationResponseListener<SimpleApnsPushNotification> {

        override fun operationComplete(
            future: PushNotificationFuture<SimpleApnsPushNotification, PushNotificationResponse<SimpleApnsPushNotification>>?
        ) {
            // When using a listener, callers should check for a failure to send a
            // notification by checking whether the future itself was successful
            // since an exception will not be thrown.
            // When using a listener, callers should check for a failure to send a
            // notification by checking whether the future itself was successful
            // since an exception will not be thrown.
            if (future!!.isSuccess) {
                val pushNotificationResponse: PushNotificationResponse<SimpleApnsPushNotification> = future.now

                // Delete non-functional tokens
                if ((!pushNotificationResponse.isAccepted) && (pushNotificationResponse.rejectionReason == "BadDeviceToken" || pushNotificationResponse.rejectionReason == "Unregistered")) {
                    val token = future.pushNotification.token
                    transaction {
                        Tables.DeviceTokens.deleteWhere {
                            Tables.DeviceTokens.token eq token
                        }
                    }
                }

                // Handle the push notification response as before from here.
            } else {
                // Something went wrong when trying to send the notification to the
                // APNs gateway. We can find the exception that caused the failure
                // by getting future.cause().
                future.cause().printStackTrace()
            }
        }

    }
}
