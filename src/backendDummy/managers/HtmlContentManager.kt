package net.toomuchram.backendDummy.managers

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.owasp.html.HtmlPolicyBuilder


class HtmlContentManager {

    private val entityIDConverter = EntityIDConverter()

    /**
     * Function to insert content into the content database
     * If the name is already found within the database, update the corresponding content
     * If not, insert the content into the database
     * @param name The label that should be attached to the content, such as 'Info' and 'Programme'
     * @param content The content itself
     */
    fun insertContent(name: String, content: String, excursionId: Int) {

        val policy = HtmlPolicyBuilder()
            .allowElements("a", "h1", "h2", "h3", "h4", "ul", "li", "ol", "br", "p", "i", "b", "u", "pre")
            .allowUrlProtocols("https", "tel", "mailto")
            .allowAttributes("href").onElements("a")
            .requireRelNofollowOnLinks()
            .toFactory()
        val safeContent = policy.sanitize(content)

        val contentFound = transaction {
            Tables.HtmlContent.select {
                (Tables.HtmlContent.name eq name) and (Tables.HtmlContent.excursionId eq excursionId)
            }.count() > 0
        }

        // Check if the content is already present in the database
        if(contentFound) {
            // If so, alter the content present
            transaction {
                Tables.HtmlContent.update({( Tables.HtmlContent.name eq name ) and (Tables.HtmlContent.excursionId eq excursionId)}) {
                    it[Tables.HtmlContent.content] = safeContent
                }
            }
        } else {
            // If not, insert new content into the database
            transaction {
                Tables.HtmlContent.insert {
                    it[Tables.HtmlContent.name] = name
                    it[Tables.HtmlContent.content] = safeContent
                    it[Tables.HtmlContent.excursionId] = entityIDConverter.getExcursionEntityID(excursionId)
                }
            }
        }

    }

    /**
     * Function to read content from the HtmlContent database
     * @param name The label of the content
     * @return The content, or if it couldn't be found, null
     */
    fun readContent(name: String, excursionId: Int): String? {
        return transaction {
            val query = Tables.HtmlContent.select {
                (Tables.HtmlContent.name eq name) and (Tables.HtmlContent.excursionId eq excursionId)
            }
            if (query.count() > 0) {
                return@transaction query.elementAt(0)[Tables.HtmlContent.content]
            } else {
                return@transaction null
            }
        }
    }
}