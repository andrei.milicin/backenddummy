package net.toomuchram.backendDummy.managers

import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

class ExcursionManager() {
    fun getAllExcursion(): List<Models.Excursion> {
        val excursions = mutableListOf<Models.Excursion>()

        transaction {
            Tables.Excursions.selectAll().forEach { row ->
                excursions.add(
                    Models.Excursion(
                        row[Tables.Excursions.id].value,
                        row[Tables.Excursions.name]
                    )
                )
            }
        }

        return excursions
    }

    fun addExcursion(name: String): Int {
        return transaction {
            return@transaction Tables.Excursions.insertAndGetId {
                it[Tables.Excursions.name] = name
            }.value
        }
    }
}