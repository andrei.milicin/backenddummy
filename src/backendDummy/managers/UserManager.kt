package net.toomuchram.backendDummy.managers

import net.toomuchram.backendDummy.Hasher
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import java.lang.Exception


// The authentication manager class manages the Users table
class UserManager {

    private val entityIDConverter = EntityIDConverter()


    /**
     * Function to check credentials against database-stored credentials
     * @param user The user to be checked (contains username + password)
     * @return The user ID, if the credentials are right
     * @throws Exceptions.NoSuchUserException The given user doesn't exist
     * @throws Exceptions.WrongPasswordException The password is wrong
     */
    fun login(user: Models.User): Int {

        return transaction {
            val matchingusers = Tables.Users.slice(
                Tables.Users.password, Tables.Users.id
            ).select {
                (Tables.Users.username eq user.username) and (Tables.Users.excursion eq user.excursion)
            }
            when {
                matchingusers.count() == 1 -> {
                    val pwHash = matchingusers.elementAt(0)[Tables.Users.password]
                    val userId = matchingusers.elementAt(0)[Tables.Users.id].value

                    if (BCryptPasswordEncoder().matches(Hasher().sha512(user.password), pwHash)) {
                        return@transaction userId
                    } else {
                        throw Exceptions.WrongPasswordException(
                            "Wrong password"
                        )
                    }
                }
                matchingusers.count() < 1 -> {
                    throw Exceptions.NoSuchUserException("No such user found")
                }
                else -> {
                    throw Exception("More than one user found")
                }
            }
        }
    }

    fun register(user: Models.User, admin: Boolean): Int {

        // Check if username and password are already taken
        transaction {
            val query: Query = Tables.Users.select {
                Tables.Users.username eq user.username and
                        (Tables.Users.excursion eq user.excursion)
            }
            if (query.count() > 0) {
                throw Exceptions.UsernameTakenException("Username has already been taken")
            }
        }

        val excursionEntityID = entityIDConverter.getExcursionEntityID(user.excursion)

        return transaction {
            return@transaction Tables.Users.insertAndGetId {
                it[username] = user.username
                it[password] = BCryptPasswordEncoder().encode(
                    Hasher().sha512(user.password)
                )
                it[Tables.Users.admin] = admin
                it[excursion] = excursionEntityID
            }
        }.value
    }

    fun isAdmin(userId: Int): Boolean {
        return transaction {
            val query: Query = Tables.Users.slice(
                Tables.Users.admin
            ).select {
                Tables.Users.id eq userId
            }
            if (query.count() < 1) {
                throw Exceptions.NoSuchUserException("No such user found")
            }
            return@transaction query.elementAt(0)[Tables.Users.admin]
        }
    }

    /**
     * Function to retrieve the username from a user ID
     * @param userId The user ID
     * @return The username
     */
    fun getUsername(userId: Int): String {
        return transaction {
            val matchingUsers = Tables.Users.slice(
                Tables.Users.username
            ).select {
                Tables.Users.id eq userId
            }
            if (matchingUsers.count() < 1) {
                throw Exceptions.NoSuchUserException("No such user found")
            } else {
                return@transaction matchingUsers.elementAt(0)[Tables.Users.username]
            }
        }
    }

    /**
     * Function to get a list of admin usernames
     * @return A list of admin usernames
     */
    fun getAllAdmins(excursionId: Int): List<Int> {
        return transaction {
            val userIds = mutableListOf<Int>()
            Tables.Users
                .slice(Tables.Users.id)
                .select {
                    (Tables.Users.admin eq true) and (Tables.Users.excursion eq excursionId)
                }.map {
                    userIds.add(it[Tables.Users.id].value)
                }
            return@transaction userIds
        }
    }

    /**
     * Get the userIDs of all users belonging to a particular excursion
     * @param excursionId The ID of the excursion
     * @return A list of user IDs
     */
    fun getAllUsers(excursionId: Int): List<Int> {
        return transaction {
            val users = mutableListOf<Int>()
            Tables.Users.slice(Tables.Users.id).select { Tables.Users.excursion eq excursionId }
                .forEach { row ->
                    users.add(
                        row[Tables.Users.id].value
                    )
                }

            return@transaction users
        }
    }

    fun getExcursion(userId: Int): Int {
        return transaction {
            val users =  Tables.Users.slice(Tables.Users.excursion)
                .select{ Tables.Users.id eq userId }
            if (users.empty()) {
                throw Exceptions.NoSuchUserException("No such user found")
            }
            return@transaction users.elementAt(0)[Tables.Users.excursion].value

        }
    }

}