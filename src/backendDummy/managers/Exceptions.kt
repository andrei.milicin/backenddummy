package net.toomuchram.backendDummy.managers

class Exceptions {
    class AlreadyExistsException(message: String) : Exception(message)
    class NoSuchTeamCategoryException(message: String) : Exception(message)
    class NoSuchTeamException(message: String) : Exception(message)
    class UsernameTakenException(message: String) : java.lang.Exception(message)
    class NoSuchUserException(message: String) : java.lang.Exception(message)
    class WrongPasswordException(message: String) : java.lang.Exception(message)
    class NoSuchSessionException(message: String): Exception(message)
    class NoSuchExcursionException(message: String): Exception(message)
}