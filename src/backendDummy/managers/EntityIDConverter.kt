package net.toomuchram.backendDummy.managers

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

/**
 * Class for internal Manager use only
 * Not to be used by anything outside net.toomuchram.backendDummy.managers
 */
class EntityIDConverter {
    fun getUserEntityID(userId: Int): EntityID<Int> {
        return transaction {
            val users = Tables.Users
                .slice(Tables.Users.id)
                .select {
                    (Tables.Users.id eq userId)
                }

            if (users.empty()) {
                throw Exceptions.NoSuchUserException("No such user found!")
            }

            return@transaction users.elementAt(0)[Tables.Users.id]
        }
    }

    fun getTeamEntityID(teamId: Int): EntityID<Int> {
        return transaction {
            val teams = Tables.Teams.slice(Tables.Teams.id).select {Tables.Teams.id eq teamId}
            if (teams.empty()) {
                throw Exceptions.NoSuchTeamException("No such team found!")
            }
            return@transaction teams.elementAt(0)[Tables.Teams.id]
        }
    }

    fun getTeamCategoryEntityID(categoryId: Int): EntityID<Int> {
        return transaction {
            val categories = Tables.TeamCategories.slice(Tables.TeamCategories.id).select {Tables.TeamCategories.id eq categoryId}
            if (categories.empty()) {
                throw Exceptions.NoSuchTeamCategoryException("No such team category found!")
            }
            return@transaction categories.elementAt(0)[Tables.TeamCategories.id]
        }
    }

    fun getExcursionEntityID(excursionId: Int): EntityID<Int> {
        return transaction {
            val categories = Tables.Excursions.slice(Tables.Excursions.id).select {Tables.Excursions.id eq excursionId}
            if (categories.empty()) {
                throw Exceptions.NoSuchExcursionException("No such excursion found!")
            }
            return@transaction categories.elementAt(0)[Tables.Excursions.id]
        }
    }
}