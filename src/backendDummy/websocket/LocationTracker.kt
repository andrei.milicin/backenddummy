package net.toomuchram.backendDummy.websocket


class LocationTracker {
    private var cachedLocations = HashMap<Int, HashMap<Int, Location>>()

    fun addLocation(userId: Int, excursionId: Int, incomingLocation: Location) {
        if (cachedLocations[excursionId] == null) {
            cachedLocations[excursionId] = hashMapOf()
        }
        cachedLocations[excursionId]!![userId] = incomingLocation
    }

    fun getLocation(userId: Int, excursionId: Int): Location? {
        return cachedLocations[excursionId]?.get(userId)
    }

    fun getAllLocations(excursionId: Int): List<Location> {
        val locations = mutableListOf<Location>()
        cachedLocations[excursionId]?.forEach { (_, location) ->
            locations.add(location)
        }
        return locations
    }
}

data class Location (
    val username: String,
    val lat: Double,
    val lon: Double,
    val channel: String = "locations",
    val time: String
)