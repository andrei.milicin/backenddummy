package net.toomuchram.backendDummy.websocket

import org.java_websocket.WebSocket

class WebsocketSessionRegistry {
    // Pair: websocketSession + sessionId
    val sessions = HashMap<WebSocket, WSSessionMetadata>()

    fun addSession(wsSession: WebSocket, metadata: WSSessionMetadata) {
        sessions[wsSession] = metadata
    }

    fun removeSession(wsSession: WebSocket) {
        sessions.remove(wsSession)
    }

}

data class WSSessionMetadata (
    val userId: Int,
    val sessionId: String,
    val excursionId: Int
)