package net.toomuchram

import net.toomuchram.backendDummy.responders.announcements
import net.toomuchram.backendDummy.responders.staticRequests
import net.toomuchram.backendDummy.responders.*
import net.toomuchram.backendDummy.managers.SessionManager
import com.github.mustachejava.DefaultMustacheFactory
import com.turo.pushy.apns.ApnsClient
import com.turo.pushy.apns.ApnsClientBuilder
import io.ktor.application.Application
import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.features.ContentNegotiation
import io.ktor.features.DoubleReceive
import io.ktor.http.*
import io.ktor.jackson.JacksonConverter
import io.ktor.mustache.Mustache
import io.ktor.request.httpMethod
import io.ktor.request.path
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.routing.routing
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.toomuchram.backendDummy.TUI
import net.toomuchram.backendDummy.managers.*
import net.toomuchram.backendDummy.websocket.LocationTracker
import net.toomuchram.backendDummy.websocket.RomereisWebsocket
import net.toomuchram.backendDummy.websocket.WebsocketSessionRegistry
import org.jetbrains.exposed.sql.Database
import java.io.File
import java.io.FileNotFoundException
import java.net.InetSocketAddress
import kotlin.system.exitProcess

fun main(args: Array<String>) {

    val configManager = ConfigManager()
    /* Initialise Database */
    val databaseConfig = configManager.readDatabaseConfig()
    Database.connect(
        "jdbc:mysql://${databaseConfig.host}/${databaseConfig.database}",
        driver = "com.mysql.cj.jdbc.Driver",
        user = databaseConfig.username, password = databaseConfig.password
    )

    if (args.contains("--create-tables")) {
        TUI().createTables()
        exitProcess(0)
    }

    if (args.contains("--create-excursion")) {
        TUI().createExcursion(args)
        TUI().createTables()
        exitProcess(0)
    }

    val port = configManager.readWebserverConfig().port

    // It's dirty, I know
    // But guess what, it works
    val ktorArgs: Array<String> = arrayOf("-port=$port")
    io.ktor.server.netty.EngineMain.main(ktorArgs)
}


@Suppress("unused") // io.ktor.server.netty.EngineMainReferenced in application.conf
@JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(CORS) {
        method(HttpMethod.Options)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        method(HttpMethod.Patch)
        anyHost() // @TODO: Don't do this in production if possible. Try to limit it.
    }

    install(ContentNegotiation) {
        register(ContentType.Application.Json, JacksonConverter())
    }

    install(Mustache) {
        mustacheFactory = DefaultMustacheFactory("templates")
    }

    install(DoubleReceive)


    val configManager = ConfigManager()

    /* Initialise the session manager */
    val sessionManager = SessionManager()

    /* Initialise authentication request responder */
    val userManager = UserManager()


    val apnsClient: ApnsClient? = try {
        /* APNs initialisation */
        val apnsConfig = configManager.readAPNsConfig()
        val apnsCertificatePath = apnsConfig.certificatePath
        val apnsCertificateKey = apnsConfig.certificateKey

        val apnsHost = if(apnsConfig.production) {
            ApnsClientBuilder.PRODUCTION_APNS_HOST
        } else {
            println("Using APNs development endpoint")
            ApnsClientBuilder.DEVELOPMENT_APNS_HOST
        }

        ApnsClientBuilder()
            .setApnsServer(apnsHost)
            .setClientCredentials(File(apnsCertificatePath), apnsCertificateKey)
            .build()

    } catch (e: FileNotFoundException) {
        println("Could not initialise APNs. Push notifications will not be sent!")
        null
    }

    /* Initialise announcement manager */
    val announcementManager = AnnouncementManager(configManager, apnsClient)

    /* Initialise chat manager */
    val chatManager = ChatManager()


    /* Initialise LocationTracker */
    val locationTracker = LocationTracker()

    /* Initialise websocket session tracker */
    val defaultWebsocketSessionRegistry = WebsocketSessionRegistry()

    /* Initialise htmlcontent manager */
    val htmlContentManager = HtmlContentManager()

    /* Initialise admin task manager */
    val adminTasks = AdminTasks(sessionManager, announcementManager)

    /* Initialise team manager */
    val teamsManager = TeamsManager(userManager)

    val excursionManager = ExcursionManager()

    val wsPort = configManager.readWebSocketPort()
    val websocketServer = RomereisWebsocket(
        InetSocketAddress(wsPort),
        defaultWebsocketSessionRegistry,
        sessionManager, userManager,
        locationTracker,
        teamsManager
    )

    val managers = Managers(
        adminTasks,
        announcementManager,
        chatManager,
        configManager,
        excursionManager,
        htmlContentManager,
        sessionManager,
        userManager,
        teamsManager,
        websocketServer
    )

    val helpers = Helpers(managers)

    CoroutineScope(Dispatchers.IO).launch {
        websocketServer.run()
    }




    routing {

        /**
         * Verify the session ID and session first
         * This is done centrally as to avoid duplicate code
         * If a location should not be checked for a session ID, add it to the unprotected array
         */
        intercept(ApplicationCallPipeline.Features) {

            val unprotected = mutableListOf(
                "/",
                "/index.html",
                "/css/main.css",
                "/css/normalize.css",
                "/download",
                "/img/contact.png",
                "/img/screenshot.png",
                "/js/main.js",
                "/404.html",
                "/contact.html",
                "/favicon.ico",
                "/humans.txt",
                "/icon.png",
                "/privacy.html",
                "/robots.txt",

                "/status",
                "/countdown",
                "/login",
                "/register",
                "/logout",
                "/verifySession",
                "/sendMessage" /* Because multipart + doublereceive doesn't work*/,
                "/excursions"
            )
            unprotected.map { path ->
                if (path == call.request.path()) {
                    return@intercept proceed()
                }
            }

            val parameters: Parameters = when (call.request.httpMethod) {
                HttpMethod.Post -> {
                    call.receiveParameters()
                }
                else -> {
                    call.request.queryParameters
                }
            }

            if (!parameters.contains("sessionId")) {
                call.respond(HttpStatusCode.BadRequest, "ERR_MISSING_SESSIONID")
                return@intercept finish()
            }

            val sessionId = parameters["sessionId"]!!
            if (!sessionManager.verifySession(sessionId)) {
                call.respond(HttpStatusCode.Unauthorized, "ERR_NO_SUCH_SESSION")
                return@intercept finish()
            }
            proceed()
        }

        /**
         * Static requests
         * Such as countdown and status
         */
        staticRequests(managers)

        /**
         * Announcements and such
         */
        announcements(managers, helpers)

        /**
         * Sessions & login
         */
        login(managers, helpers)
        sessions(managers)

        /**
         * Messaging
         */
        messaging(managers, helpers)

        /**
         * Location endpoint
         */
        locations(locationTracker, managers)

        /**
         * Dynamic content
         */
        dynamicContent(managers, helpers)

        /**
         * Admin Console
         */
        admin(managers, helpers)

        /**
         * Teams
         */
        teams(managers, helpers)

        /**
         * Excursions
         */
        excursions(managers)
    }
}